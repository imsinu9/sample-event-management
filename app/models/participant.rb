class Participant < ApplicationRecord
  extend Enumerize

  enumerize :status, in: [:yes, :no, :maybe], default: :yes

  belongs_to :user
  belongs_to :event

  before_create :check_overlapping_events

  private

  def check_overlapping_events
    return true unless status == :yes

    user.events
  end
end