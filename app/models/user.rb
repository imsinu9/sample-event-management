class User < ApplicationRecord
  # devise :database_authenticatable, :registerable, :validatable

  has_many :participations

  validates :username, presence: true
  validates :email, presence: true
  validates :phone, presence: true
end
