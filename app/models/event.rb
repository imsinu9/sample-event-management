class Event < ApplicationRecord
  extend Enumerize

  enumerize :status, in: [:created, :active, :completed], default: :created

  validates :title, presence: true
  validates :event_start_time, presence: true
  validates :event_end_time, presence: true

  has_many :user, through: :participant

  before_save :set_status

  private

  def past_event?
    Time.now >= event_end_time
  end

  def set_status
    status = :completed if past_event?
  end
end
