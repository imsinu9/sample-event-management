module BulkImport
  class ImportService

    def initialize(file_path)
      @file_path = file_path || default_seed_path
    end

    private

    def default_seed_path
      raise NotImplementedError
    end

    def whitelisted_columns
      raise NotImplementedError
    end
  end
end