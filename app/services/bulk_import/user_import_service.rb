require 'csv'

module BulkImport
  class UserImportService < ImportService

    def initialize(file_path)
      super
    end

    def import
      users = []

      CSV.foreach(@file_path, headers: true, encoding: 'ISO-8859-1') do |row|
        users << row
      end

      User.import(whitelisted_columns, users, batch_size: 100)
    end

    private

    def default_seed_file
      'public/users.csv'
    end

    def whitelisted_columns
      %i(username email phone)
    end
  end
end