require 'csv'

module BulkImport
  class EventImportService < ImportService

    def initialize(file_path)
      super
    end

    def import
      events = []

      CSV.foreach(@file_path, headers: true, encoding: 'ISO-8859-1') do |row|
        # Set Endtime for all day event
        row['endtime'] = set_to_end_of_day(row['starttime']) if all_day_event? row['allday']

        # Parse and populate participation creation service
        participation_creation_service.add(row['title'], row['users#rsvp'])

        events << row
      end

      Event.import(whitelisted_columns, events, batch_size: 100)
      participation_creation_service.create
    end

    private

    def default_seed_file
      'public/events.csv'
    end

    def whitelisted_columns
      %i(title event_start_time event_end_time description)
    end

    # CSV expects allday to be string
    def all_day_event?(flag)
      flag == 'TRUE'
    end

    def set_to_end_of_day(startime)
      DateTime.parse(startime).end_of_day
    end

    def participation_creation_service
      @participant_creation_service ||= BulkParticipantCreationService.new
    end
  end
end