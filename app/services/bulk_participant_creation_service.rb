class BulkParticipantCreationService
  attr_reader :participants

  def initialize
    @participants = []
  end

  #Sample
  # => aletha#maybe;samanta_vandervort#no;luisa_lebsack#yes;
  def add(event_title, participants_response)
    parse_responses(participants_response).map do |response|
      username, status = response.split('#')
      @participants << [event_title, username, status]
    end
  end

  def create
    Participant.import(whitelisted_columns, @participants)
  end

  private

  def parse_responses(responses)
    responses.split(';')
  end

  def whitelisted_columns
    %i(user_id event_id status)
  end
end
