class CreateUserEventsTable < ActiveRecord::Migration[5.2]
  def change
    create_table :events do |t|
      t.string :title, null: false
      t.string :description
      t.string :status, null: false
      t.datetime :event_start_time, null: false
      t.datetime :event_end_time, null: false
      t.timestamps
    end

    create_table :users do |t|
      t.string :username, null: false, default: '', index: true
      t.string :email, null: false, default: ''
      t.string :phone

      t.timestamps
    end

    create_table :participants do |t|
      t.integer :event_id, null: false
      t.integer :user_id, null: false
      t.string :status, null: false
    end

    # create_table :moderators do |t|
    #   t.integer :event_id
    #   t.integer :user_id
    #   t.boolean :active, null: false, default: false
    # end

    add_index :users, :email, unique: true
  end
end
